﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SqlSugar;

namespace Analysis.IRepository
{
    public interface IBaseRepository<T> where T: class,new()
    {
        Task<bool> CreateAsync(T entity);
        Task<bool> CreateListAsync(List<T> entity);
        Task<bool> DeleteAsync(int id);
        Task<bool> EditAsync(T entity);
        Task<T> FindAsync(int id);
        /// <summary>
        /// 自定义查找
        /// </summary>
        /// <param name="func"></param>
        /// <returns></returns>
        Task<T> FindAsync(Expression<Func<T,bool>> func);
        /// <summary>
        /// 查找所有数据
        /// </summary>
        /// <returns></returns>
        Task<List<T>> QueryAsync();
        /// <summary>
        /// 自定义条件查询
        /// </summary>
        /// <param name="func"></param>
        /// <returns></returns>
        Task<List<T>> QueryAsync(Expression<Func<T,  bool>> func);
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        Task<List<T>> QueryAsync(int page,int size,RefAsync<int> total);

        /// <summary>
        /// 自定义条件分页查询
        /// </summary>
        /// <param name="func"></param>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        Task<List<T>> QueryAsync(Expression<Func<T, bool>> func, int page, int size, RefAsync<int> total);
    }
}
