﻿using Analysis.Model;

namespace Analysis.IRepository
{
    public interface ITelemetryRepository:IBaseRepository<Telemetry>
    {
    }
}
