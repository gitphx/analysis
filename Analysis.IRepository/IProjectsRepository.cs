﻿using Analysis.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analysis.IRepository
{
    public interface IProjectsRepository:IBaseRepository<Projects>
    {

    }
}
