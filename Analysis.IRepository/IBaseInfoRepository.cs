﻿using Analysis.Model;

namespace Analysis.IRepository
{
    public interface IBaseInfoRepository: IBaseRepository<BaseInfo>
    {
    }
}
