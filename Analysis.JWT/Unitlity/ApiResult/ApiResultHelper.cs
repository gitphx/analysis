﻿using SqlSugar; 

namespace Analysis.JWT.Unitlity.ApiResult
{
    public static class ApiResultHelper
    {
        /// <summary>
        /// 成功
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static ApiResult Sueecss(dynamic data)
        {
            return new ApiResult
            {
                Code = 200,
                Data = data,
                Msg = "操作成功",
                Total = 0
            };
        }
        /// <summary>
        /// 分页-成功
        /// </summary>
        /// <param name="data"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        public static ApiResult Sueecss(dynamic data,RefAsync<int> total)
        {
            return new ApiResult
            {
                Code = 200,
                Data = data,
                Msg = "操作成功",
                Total = total
            };
        }
        /// <summary>
        /// 失败
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static ApiResult Error(string msg)
        {
            return new ApiResult
            {
                Code = 500,
                Data = null,
                Msg = msg,
                Total = 0
            };
        }
    }
}
