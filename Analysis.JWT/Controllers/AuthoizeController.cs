﻿using Analysis.IService;
using Analysis.JWT.Unitlity.ApiResult;
using Analysis.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Analysis.JWT.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthoizeController:ControllerBase
    {
        private readonly IProjectsService _iProjectsService;
        public AuthoizeController(IProjectsService iProjectsService)
        {
            _iProjectsService = iProjectsService;
        }

        /// <summary>
        /// 自定义查询
        /// </summary>
        /// <param name="ch"></param>
        /// <param name="en"></param>
        /// <returns></returns>
        [HttpPost("Login")]
        public async Task<ActionResult<ApiResult>> Login(string ch, string en, int len, string frame) 
        {
            // 数据校验
            var res = await _iProjectsService.FindAsync(c => c.Pch == ch);
            if (res == null) return ApiResultHelper.Error("项目不存在");
            // this.User.Identity.Name  代替【张三】
            // 校验成功| 【登录成功】
            var claims = new Claim[]
                {
                    new Claim(ClaimTypes.Name, res.Pch),
                    new Claim("ID", res.Id.ToString()),
                    new Claim("EN", res.Pen.ToString()),
                };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("SDMC-CJASI-SAD-DFSFA-SADHJVF-VF")); // 权鉴与webapi 密钥
            // issuer 代表颁发Token的web应用程序，audience是Token的受理者
            var token = new JwtSecurityToken(
                    issuer: "http://localhost:6060",   // 权鉴地址
                    audience: "http://localhost:81",  // webapi地址
                    claims: claims,
                    notBefore:DateTime.Now,
                    expires:DateTime.Now.AddHours(1),       // 过期时间
                    signingCredentials: new SigningCredentials(key,SecurityAlgorithms.HmacSha256)
                );
            var jwtToken = new JwtSecurityTokenHandler().WriteToken(token);
            return ApiResultHelper.Sueecss(jwtToken); 

            //Projects p = new Projects
            //{
            //    Pch = ch,
            //    Pen = en,
            //    PackageLength = len,
            //    PframeHander = frame
            //};

            //bool b = await _iProjectsService.CreateAsync(p);
            //if (!b) return ApiResultHelper.Error("添加失败");
            //return ApiResultHelper.Sueecss(b);

            //return ApiResultHelper.Sueecss(res);

        }

    }
}
