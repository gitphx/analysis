﻿using System;
using SqlSugar;

namespace Analysis.Model    
{
    [SugarTable("Telemetry", TableDescription = "TableTelemetry")]
    public class Telemetry:BaseId
    {
        [SugarColumn(ColumnDataType = "Nvarchar(20)")]
        public string Tno { get; set; }

        [SugarColumn(ColumnDataType = "Nvarchar(50)")]
        public string Tname { get; set; }

        [SugarColumn(ColumnDataType = "Nvarchar(13)")]
        public string Tvalue { get; set; }

        [SugarColumn(ColumnDataType = "Nvarchar(30)", IndexGroupNameList = new string[] { "Tstime" })]
        public string Tstime { get; set; }

        [SugarColumn(ColumnDataType = "Nvarchar(30)")]
        public string Tstatus { get; set; }


        [SugarColumn(ColumnDataType = "Nvarchar(10)")]
        public string Tunit { get; set; }


        [SugarColumn(IndexGroupNameList = new string[]{ "SubidGroup" })]
        public int Subid { get; set; }

        [SugarColumn(IndexGroupNameList = new string[] { "MidGroup" })]
        public int Mid { get; set; }




        [SugarColumn(ColumnDataType = "DATETIME(3) ")]
        public DateTime CreateTime { get; set; }
    }
}
