﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analysis.Model
{ 
    [SugarTable("BaseInfo", TableDescription = "BaseInfoTable")]
    public class BaseInfo:BaseId
    {
        [SugarColumn(ColumnDataType = "int(11)")]
        public int Pid { get; set; } //所属项目
        [SugarColumn(ColumnDataType = "int(11)")]
        public int Subid { get; set; } //所属模块
        [SugarColumn(ColumnDataType = "Nvarchar(30)")]
        public string ChannelNumber { get; set; } //波道号
        [SugarColumn(ColumnDataType = "Nvarchar(50)")]
        public string Name { get; set; } //名称
        [SugarColumn(ColumnDataType = "Nvarchar(20)")]
        public string No { get; set; } //代号
        [SugarColumn(ColumnDataType = "Nvarchar(30)")]
        public string SourceCode { get; set; } //源码

        [SugarColumn(ColumnDataType = "int(11)", IsNullable = true)]
        public int Formula { get; set; }  //公式
        [SugarColumn(ColumnDataType = "Nvarchar(30)", IsNullable = true)]
        public string FormulaCoefficient { get; set; }//公式系数

        [SugarColumn(ColumnDataType = "int(11)", IsNullable = true)]
        public int KeepDecimal { get; set; } // 小数位数
        [SugarColumn(ColumnDataType = "Nvarchar(30)", IsNullable = true)]
        public string HandleAmount { get; set; } //量
        [SugarColumn(ColumnDataType = "Nvarchar(255)", IsNullable = true)]
        public string ShowMsg { get; set; } //显示信息 

        [SugarColumn(ColumnDataType = "char(4)",IsNullable =true)]
        public char IsWarning { get; set; }//是否报警
        [SugarColumn(ColumnDataType = "Nvarchar(10)", IsNullable = true)]
        public string Unit { get; set; }  //单位
        [SugarColumn(ColumnDataType = "Nvarchar(255)", IsNullable = true)]
        public string NormalRange { get; set; } //正常范围
        [SugarColumn(ColumnDataType = "Nvarchar(30)", IsNullable = true)]
        public string WarningRange { get; set; }  //报警值范围
        [SugarColumn(ColumnDataType = "Nvarchar(10)", IsNullable = true)]
        public string Package { get; set; }  //所属包
        [SugarColumn(ColumnDataType = "DATETIME(3) ", IsNullable = true)]
        public DateTime CreateTime { get; set; } //创建日期

    }
}
