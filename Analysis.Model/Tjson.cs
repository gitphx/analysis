﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analysis.Model
{ 
    [SplitTable(SplitType.Day)] 
    [SugarTable("Tjson_{year}{month}{day}", TableDescription = "Tjson_{year}{month}{day}")] 
    public  class Tjson
    {
        [SugarColumn(IsPrimaryKey = true)]
        public long Id { get; set; }

        /// <summary>
        /// 遥测代号
        /// </summary>
        public string No { get; set; }

        /// <summary>
        /// 遥测参数名称
        /// </summary>
        [SugarColumn(ColumnDataType = "Nvarchar(30)")]
        public string Name { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        [SugarColumn(ColumnDataType = "Nvarchar(20)")]
        public string Value { get; set; }

   
        /// <summary>
        /// 单位
        /// </summary>

        [SugarColumn(ColumnDataType = "Nvarchar(10)")]
        public string Unit { get; set; }

        [SplitField] //分表字段 在插入的时候会根据这个字段插入哪个表，在更新删除的时候用这个字段找出相关表

        /// <summary>
        /// 状态
        /// </summary>
        public string State { get; set; }

         
        public int Subid { get; set; }
         
        public int Mid { get; set; }
        public DateTime CreateTime { get; set; }

    }
}
