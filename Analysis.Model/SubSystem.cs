﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar;

namespace Analysis.Model
{
    [SugarTable("SubSystem", TableDescription = "TableSubSystem")]
    public class SubSystem:BaseId
    {
        [SugarColumn(ColumnDataType = "Nvarchar(30)")]
        public string Sch { get; set; }

        [SugarColumn(ColumnDataType = "Nvarchar(30)")]
        public string Sen { get; set; }

        [SugarColumn(ColumnDataType = "int(11)")]
        public int Sid { get; set; }

        [SugarColumn(IsIgnore = true)]
        public List<Machine> MachineInfo { get; set; }


        [SugarColumn(IsIgnore = true)]
        public List<Telemetry> TelemetryInfo { get; set; }
    }
}
