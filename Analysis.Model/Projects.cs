﻿using SqlSugar;

namespace Analysis.Model
{
    public class Projects:BaseId
    {
        [SugarColumn(ColumnDataType = "Nvarchar(30)", ColumnDescription="项目中文名称")]
        public string Pch { get; set; }

        [SugarColumn(ColumnDataType = "Nvarchar(30)", ColumnDescription = "项目英文名称")]
        public string Pen { get; set; }

        [SugarColumn(ColumnDataType = "int(11)", ColumnDescription = "包长度")]
        public int PackageLength { get; set; }

        [SugarColumn( ColumnDataType = "Nvarchar(20)", ColumnDescription = "帧头")]//可以为NULL
        public string PframeHander { get; set; }
    }
}
