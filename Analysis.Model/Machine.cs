﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar;

namespace Analysis.Model
{
    [SugarTable("Machine", TableDescription = "TableMachine")]
    public class Machine:BaseId
    {
        [SugarColumn(ColumnDataType = "Nvarchar(30)")]
        public string Mch { get; set; }

        [SugarColumn(ColumnDataType = "Nvarchar(30)")]
        public string Men { get; set; }

        [SugarColumn(ColumnDataType = "int(11)")]
        public int Msid { get; set; }
    }
}
