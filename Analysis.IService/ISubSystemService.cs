﻿using Analysis.Model;

namespace Analysis.IService
{
    public interface ISubSystemService:IBaseService<SubSystem>
    {
    }
}
