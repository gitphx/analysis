﻿using Analysis.IService;
using Analysis.Model;
using Analysis.Unitlity.ApiResult;
using Microsoft.AspNetCore.Mvc;
using SqlSugar;
using System.Threading.Tasks;

namespace Analysis.Controllers
{

    [Controller]
    [Route("api/[controller]")]
    public class BaseInfoController : ControllerBase
    {
        private readonly IBaseInfoService _iBaseInfoService;
        public BaseInfoController(IBaseInfoService iBaseInfoService)
        {
            _iBaseInfoService = iBaseInfoService;
        }

        [HttpGet("GetBaseInfo")]
        public async Task<ActionResult<ApiResult>> GetBaseInfo()
        {
            var b = await _iBaseInfoService.QueryAsync();
            if (b.Count == 0) return ApiResultHelper.Error("没有更多的数据");
            return ApiResultHelper.Sueecss(b);

        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="no">遥测id</param>
        /// <param name="name">名称</param>
        /// <param name="val">值</param>
        /// <param name="status">状态</param>
        /// <param name="unit">单位</param>
        /// <param name="_sid">所属分系统id</param>
        /// <param name="mid">所属单机</param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<ActionResult<ApiResult>> Create(int _Pid,int _Subid, string _ChannelNumber, string _Name, string _No, string _SourceCode)
        {
            BaseInfo s = new BaseInfo
            {
                Pid = _Pid,
                Subid = _Subid,
                ChannelNumber = _ChannelNumber,
                Name = _Name,
                No = _No,
                SourceCode = _SourceCode, 
            };

            bool b = await _iBaseInfoService.CreateAsync(s);
            if (!b) return ApiResultHelper.Error("添加失败");
            return ApiResultHelper.Sueecss(b);
        }


        [HttpPost("CreateALL")]
        public async Task<ActionResult<ApiResult>> CreateALL(int _Pid, int _Subid, string _ChannelNumber, string _Name, string _No,
                                                            string _SourceCode, int _Formula, string _FormulaCoefficient, int _KeepDecimal,string _HandleAmount,
                                                            string _ShowMsg, char _IsWarning,string _Unit, string _NormalRange,string _WarningRange,
                                                            string _Package)
        {

            BaseInfo s = new BaseInfo
            {
                Pid = _Pid,
                Subid = _Subid,
                ChannelNumber = _ChannelNumber,
                Name = _Name,
                No = _No,
                SourceCode = _SourceCode,
                Formula = _Formula,
                FormulaCoefficient = _FormulaCoefficient,
                KeepDecimal = _KeepDecimal,
                HandleAmount = _HandleAmount,
                ShowMsg = _ShowMsg,
                IsWarning = _IsWarning,
                Unit = _Unit,
                NormalRange = _NormalRange,
                WarningRange = _WarningRange,
                Package = _Package,
            };

            bool b = await _iBaseInfoService.CreateAsync(s);
            if (!b) return ApiResultHelper.Error("添加失败");
            return ApiResultHelper.Sueecss(b);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id">遥测id</param>
        /// <returns></returns>
        [HttpDelete("delete")]
        public async Task<ActionResult<ApiResult>> Delete(int id)
        {
            bool b = await _iBaseInfoService.DeleteAsync(id);
            if (!b) return ApiResultHelper.Error("删除失败");
            return ApiResultHelper.Sueecss(b);
        }


        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="id">遥测id</param>
        /// <param name="name">名称</param>
        /// <param name="val">值</param>
        /// <param name="status">状态</param>
        /// <param name="unit">单位</param>
        /// <param name="_sid">所属分系统id</param>
        /// <param name="mid">所属单机</param>
        /// <returns></returns>
        [HttpPut("Edit")]
        public async Task<ActionResult<ApiResult>> Edit(int id, int _Pid, int _Subid, string _ChannelNumber, string _Name, string _No, string _SourceCode)
        {
            var s = await _iBaseInfoService.FindAsync(id);
            if (s == null) return ApiResultHelper.Error("没有找到该遥测");
            s.Pid = _Pid;
            s.Subid = _Subid;
            s.ChannelNumber = _ChannelNumber;
            s.Name = _Name;
            s.No = _No;
            s.SourceCode = _SourceCode; 
            bool b = await _iBaseInfoService.EditAsync(s);
            if (!b) return ApiResultHelper.Error("修改失败");
            return ApiResultHelper.Sueecss(b);
        }
        [HttpPut("EditAll")]
        public async Task<ActionResult<ApiResult>> EditAll(int id, int _Pid, int _Subid, string _ChannelNumber, string _Name, string _No,
                                                            string _SourceCode, int _Formula, string _FormulaCoefficient, int _KeepDecimal, string _HandleAmount,
                                                            string _ShowMsg, char _IsWarning, string _Unit, string _NormalRange, string _WarningRange,
                                                            string _Package)
        {
            var s = await _iBaseInfoService.FindAsync(id);
            if (s == null) return ApiResultHelper.Error("没有找到该遥测");
            s.Pid = _Pid;
            s.Subid = _Subid;
            s.ChannelNumber = _ChannelNumber;
            s.Name = _Name;
            s.No = _No;
            s.SourceCode = _SourceCode;
            s.Formula = _Formula;
            s.FormulaCoefficient = _FormulaCoefficient;
            s.KeepDecimal = _KeepDecimal;
            s.HandleAmount = _HandleAmount;
            s.ShowMsg = _ShowMsg;
            s.IsWarning = _IsWarning;
            s.Unit = _Unit;
            s.NormalRange = _NormalRange;
            s.WarningRange = _WarningRange;
            s.Package = _Package;
            bool b = await _iBaseInfoService.EditAsync(s);
            if (!b) return ApiResultHelper.Error("修改失败");
            return ApiResultHelper.Sueecss(b);
        }


        [HttpGet("QueryPage")]
        public async Task<ActionResult<ApiResult>> QueryPage(int page, int size)
        {
            RefAsync<int> total = 0;
            var t = await _iBaseInfoService.QueryAsync(page, size, total);
            if (t == null) return ApiResultHelper.Error("没有查找到数据");
            return ApiResultHelper.Sueecss(t);
        }
    }
}
