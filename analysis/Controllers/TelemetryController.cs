﻿using Analysis.IService;
using Analysis.Model;
using Analysis.Unitlity.ApiResult;
using Microsoft.AspNetCore.Mvc;
using SqlSugar; 
using System.Threading.Tasks;

namespace Analysis.Controllers
{

    [Controller]
    [Route("api/[controller]")]
    public class TelemetryController:ControllerBase
    {
        private ITelemetryService _iTelemetryService;
        public TelemetryController(ITelemetryService iTelemetryService)
        {
            _iTelemetryService = iTelemetryService;
        }

        [HttpGet("GetSubSystem")]
        public async Task<ActionResult<ApiResult>> GetTelemetry()
        {
            var b = await _iTelemetryService.QueryAsync();
            if (b.Count == 0) return ApiResultHelper.Error("没有更多的数据");
            return ApiResultHelper.Sueecss(b);

        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="no">遥测id</param>
        /// <param name="name">名称</param>
        /// <param name="val">值</param>
        /// <param name="status">状态</param>
        /// <param name="unit">单位</param>
        /// <param name="_sid">所属分系统id</param>
        /// <param name="mid">所属单机</param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<ActionResult<ApiResult>> Create(string no, string name, string val, string starTime,  string status, string unit, int _sid, int mid)
        {
            Telemetry s = new Telemetry
            {
                Tno = no,
                Tname = name,
                Tvalue = val,
                Tstime = starTime,
                Tstatus = status,
                Tunit = unit,
                Subid = _sid,
                Mid = mid
            };

            bool b = await _iTelemetryService.CreateAsync(s);
            if (!b) return ApiResultHelper.Error("添加失败");
            return ApiResultHelper.Sueecss(b);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id">遥测id</param>
        /// <returns></returns>
        [HttpDelete("delete")]
        public async Task<ActionResult<ApiResult>> Delete(int id)
        {
            bool b = await _iTelemetryService.DeleteAsync(id);
            if (!b) return ApiResultHelper.Error("删除失败");
            return ApiResultHelper.Sueecss(b);
        }


        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="id">遥测id</param>
        /// <param name="name">名称</param>
        /// <param name="val">值</param>
        /// <param name="status">状态</param>
        /// <param name="unit">单位</param>
        /// <param name="_sid">所属分系统id</param>
        /// <param name="mid">所属单机</param>
        /// <returns></returns>
        [HttpPut("Edit")]
        public async Task<ActionResult<ApiResult>> Edit(int id, string name, string val, string status, string unit, int _sid, int mid)
        {
            var s = await _iTelemetryService.FindAsync(id);
            if (s == null) return ApiResultHelper.Error("没有找到该遥测");
            s.Tname = name;
            s.Tvalue = val;
            s.Tstatus = status;
            s.Tunit = unit;
            s.Subid = _sid;
            s.Mid = mid;
            bool b = await _iTelemetryService.EditAsync(s);
            if (!b) return ApiResultHelper.Error("修改失败");
            return ApiResultHelper.Sueecss(b);
        }


        [HttpGet("QueryPage")]
        public async Task<ActionResult<ApiResult>> QueryPage(int page, int size)
        {
            RefAsync<int> total = 0;
            var t = await _iTelemetryService.QueryAsync(page, size, total);
            if (t == null) return ApiResultHelper.Error("没有查找到数据");
            return ApiResultHelper.Sueecss(t);
        }
    }
}
