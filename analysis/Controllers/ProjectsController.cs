﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Analysis.Unitlity.ApiResult;
using Analysis.IService;
using Analysis.Model;
using Microsoft.AspNetCore.Authorization;

namespace Analysis.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    //[Authorize]
    public class ProjectsController: ControllerBase
    {
        private readonly IProjectsService _iProjectsService;

        public ProjectsController(IProjectsService iProjectsService) 
        {
            _iProjectsService = iProjectsService;
        }
        [HttpGet("GetProjects")]
        public async Task<ActionResult<ApiResult>> GetProjects()
        {
            var data = await _iProjectsService.QueryAsync();
            if (data.Count == 0) return ApiResultHelper.Error("没有更多的数据");
            return ApiResultHelper.Sueecss(data);
        }

        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="ch">中文名称</param>
        /// <param name="en">英文名称</param>
        /// <param name="packageLeng">包长度</param>
        /// <param name="fh">帧头</param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<ActionResult<ApiResult>> Create(string ch, string en,int packageLeng, string fh)
        {
            // 数据验证
            Projects p = new Projects
            {
                Pch = ch,
                Pen = en,
                PackageLength = packageLeng,
                PframeHander = fh
            };

            bool b = await _iProjectsService.CreateAsync(p);
            if (!b) return ApiResultHelper.Error("添加失败");
            return ApiResultHelper.Sueecss(b);
        }

       /// <summary>
       /// 删除
       /// </summary>
       /// <param name="id">项目ID</param>
       /// <returns></returns>
        [HttpDelete("Delete")]
        public async Task<ActionResult<ApiResult>> Delete(int id)
        {
            bool b = await _iProjectsService.DeleteAsync(id);
            if (!b) return ApiResultHelper.Error("删除失败");
            return ApiResultHelper.Sueecss(b);
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="id">项目id</param>
        /// <param name="ch">中文名称</param>
        /// <param name="en">英文名称</param>
        /// <param name="packageLeng">包长度</param>
        /// <param name="fh">帧头</param>
        /// <returns></returns>
        [HttpPut("Edit")]
        public async Task<ActionResult<ApiResult>> Edit(int id, string ch, string en, int packageLeng, string fh)
        {
            var _p = await _iProjectsService.FindAsync(id);
            if (_p == null) return ApiResultHelper.Error("没有找到对应的项目");
            _p.Pch = ch;
            _p.Pen = en;
            _p.PackageLength = packageLeng;
            _p.PframeHander = fh;
            bool b = await _iProjectsService.EditAsync(_p);
            if (!b) return ApiResultHelper.Error("修改失败");
            return ApiResultHelper.Sueecss(b);
        }

    }
}
