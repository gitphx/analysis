﻿using Analysis.IService;
using Analysis.Model;
using Analysis.Unitlity.ApiResult;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Analysis.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MachineController:ControllerBase
    {
        private IMachineService _iMachineService;
        public MachineController(IMachineService iMachineService)
        {
            _iMachineService = iMachineService;
        }

        [HttpGet("GetSubSystem")]
        public async Task<ActionResult<ApiResult>> GetSubSystem()
        {
            var b = await _iMachineService.QueryAsync();
            if (b.Count == 0) return ApiResultHelper.Error("没有更多的数据");
            return ApiResultHelper.Sueecss(b);

        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="ch">中文名称</param>
        /// <param name="en">英文名称</param>
        /// <param name="_sid">所属分系统id</param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<ActionResult<ApiResult>> Create(string ch, string en, int _sid)
        {
            Machine s = new Machine
            {
                Mch = ch,
                Men = en,
                Msid = _sid
            };

            bool b = await _iMachineService.CreateAsync(s);
            if (!b) return ApiResultHelper.Error("添加失败");
            return ApiResultHelper.Sueecss(b);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id">单机id</param>
        /// <returns></returns>
        [HttpDelete("delete")]
        public async Task<ActionResult<ApiResult>> Delete(int id)
        {
            bool b = await _iMachineService.DeleteAsync(id);
            if (!b) return ApiResultHelper.Error("删除失败");
            return ApiResultHelper.Sueecss(b);
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="id">单机id</param>
        /// <param name="ch">中文名称</param>
        /// <param name="en">英文名称</param>
        /// <returns></returns>
        [HttpPut("Edit")]
        public async Task<ActionResult<ApiResult>> Edit(int id, string ch, string en)
        {
            var s = await _iMachineService.FindAsync(id);
            if (s == null) return ApiResultHelper.Error("没有找到该单机");
            s.Mch = ch;
            s.Men = en; 
            bool b = await _iMachineService.EditAsync(s);
            if (!b) return ApiResultHelper.Error("修改失败");
            return ApiResultHelper.Sueecss(b);
        }

    }
}
