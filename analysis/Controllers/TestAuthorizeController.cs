﻿using Analysis.Unitlity._Filter;
using Analysis.Unitlity.ApiResult;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Analysis.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TestAuthorizeController:ControllerBase
    {

        [HttpGet("NoAuthorize")]
        public string NoAuthorize()
        {
            return "this is NoAuthorize";
        }
        [Authorize]
        [HttpGet("Authorize")]
        public string Authorize()
        {
            return "this is Authorize";
        }

        [TypeFilter(typeof(CustomResurceFilterAttribute))]
        [HttpGet("GetCache")]
        public IActionResult GetCache(string name)
        {
            return new JsonResult(new 
            { 
                name = name,
                page = 18,
                sex = true
            });
        }
    }
}
