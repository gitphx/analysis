﻿using Microsoft.AspNetCore.Mvc;
using Analysis.Unitlity._Excel;
using Microsoft.Extensions.Hosting.Internal;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Data;
using Analysis.Unitlity.ApiResult;
using System.Collections.Generic;
using NPOI.SS.Formula.Functions;
using Analysis.Model;
using System.Text;
using System;
using Analysis.IService;
using System.Threading.Tasks;

namespace Analysis.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ExcelHandleController : ControllerBase
    {

        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IBaseInfoService _iBaseInfoService;
        private readonly IProjectsService _iProjectsService;
        public ExcelHandleController(IWebHostEnvironment webHostEnvironment, IBaseInfoService iBaseInfoService, IProjectsService iProjectsService )
        {
            _webHostEnvironment = webHostEnvironment;
            _iBaseInfoService = iBaseInfoService;
            _iProjectsService = iProjectsService;
        }

        // 获取该项目的Excel文件路径 
        public static string pathExce = string.Empty;
        public static string pathExceParam = @"/TM.xlsx";

        [HttpGet]
        public async Task<ActionResult<ApiResult>> Test()
        {
            string content_path = _webHostEnvironment.ContentRootPath;  //D:\work\_BASE\pspb\pspb\src\pspb\
            pathExce = _webHostEnvironment.WebRootPath + pathExceParam; //D:\work\_BASE\pspb\pspb\src\pspb\wwwroot

            // TODO::还有未完成的代码
            //DirectoryInfo di = new DirectoryInfo(web_path + pathExceParam);

            int _pid = 2;
            bool b = System.IO.File.Exists(pathExce);
            //判断是否有该文件
            if (System.IO.File.Exists(pathExce))
            {
                //返回指定的路径字符串的扩展名  并将数据写入(总) 二维数组里
                string extension = Path.GetExtension(pathExce);
                //打开现有文件   进行读取

                FileStream fs;
                try
                {
                    fs = System.IO.File.OpenRead(pathExce);
                }
                catch
                {
                    return ApiResultHelper.Error("读取失败") ;
                }
                DataTable dt = ExcelHelp.ExcelToTableForXLSX(fs);

                List<BaseInfo> blList = new List<BaseInfo>();
                BaseInfo bl = new BaseInfo();
                foreach (DataRow Row in dt.Rows)
                {
                    bl.Pid = Convert.ToInt16(Row[0]);
                    bl.Subid = Convert.ToInt16(Row[1]);
                    bl.ChannelNumber = Convert.ToString(Row[3]);
                    bl.Name = Convert.ToString(Row[4]);
                    bl.No = Convert.ToString(Row[5]);
                    bl.SourceCode = Convert.ToString(Row[6]);
                    bl.Formula = Convert.ToInt16(Row[7]);
                    bl.FormulaCoefficient = Convert.ToString(Row[8]);
                    bl.KeepDecimal = Convert.ToInt16(Row[9]);
                    bl.HandleAmount = Convert.ToString(Row[10]);
                    bl.ShowMsg = Convert.ToString(Row[11]);
                    bl.IsWarning = 'a';
                    bl.Unit = Convert.ToString(Row[13]);
                    bl.NormalRange = Convert.ToString(Row[14]);
                    bl.WarningRange = Convert.ToString(Row[15]);
                    bl.Package = Convert.ToString(Row[16]); 
                    blList.Add(bl);
                }
                // 判断项目是否存在

                var _pb = await _iBaseInfoService.FindAsync(b =>b.Subid== _pid);
                if (_pb == null)
                {
                    var _b = await _iBaseInfoService.CreateListAsync(blList);
                    if (_b) return ApiResultHelper.Error("批量插入失败");
                    return ApiResultHelper.Sueecss("批量插入成功");
                }
                return ApiResultHelper.Error("已存在");

            }
            return ApiResultHelper.Error("文件路径有误");
        }

    }
}
