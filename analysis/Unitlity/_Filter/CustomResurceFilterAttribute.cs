﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Analysis.Unitlity._Filter
{
    /// <summary>
    /// 资源过滤器
    /// --- Attribute 过滤特性
    /// ---IResourceFilter asp.netCore过滤器【方法、异常、资源、授权等...】
    /// </summary>
    public class CustomResurceFilterAttribute : Attribute, IResourceFilter
    {
        private readonly IMemoryCache _cache;
        public CustomResurceFilterAttribute(IMemoryCache cache)
        {
            _cache = cache;
        }
        public void OnResourceExecuted(ResourceExecutedContext context)
        {
            string path = context.HttpContext.Request.Path;
            string route = context.HttpContext.Request.QueryString.Value;
            string key = path + route;
            _cache.Set(key, context.Result);
        }

        public void OnResourceExecuting(ResourceExecutingContext context)
        {
            string path = context.HttpContext.Request.Path; // 
            string route = context.HttpContext.Request.QueryString.Value;
            string key = path + route;
            if (_cache.TryGetValue(key,out object value))
            {
                context.Result = value as IActionResult; 
            }
        }
    }
}
