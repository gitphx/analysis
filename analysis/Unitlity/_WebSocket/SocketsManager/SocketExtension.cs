﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Analysis.Unitlity._WebSocket.SocketsManager
{
    /// <summary>
    /// socket扩展extension
    /// </summary>
    public static class SocketExtension
    {
        /// <summary>
        /// 注册服务
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddWebSocketManager(this IServiceCollection services) 
        {
            services.AddTransient<ConnectionManager>();
            foreach (var type in Assembly.GetEntryAssembly().ExportedTypes)
            {
                if (type.GetTypeInfo().BaseType == typeof(SocketHandler))
                    services.AddSingleton(type);
            }
            return services;
        }

        /// <summary>
        /// 创建服务连接
        /// </summary>
        /// <param name="app"></param>
        /// <param name="path"></param>
        /// <param name="socket"></param>
        /// <returns></returns>
        public static IApplicationBuilder MapSockets(this IApplicationBuilder app, PathString path, SocketHandler socket)
        {
            //return app.Map(path, (x) => x.UseMiddleware<SocketMiddelware>(socket));
            return app.Map(path, (x) => x.UseMiddleware<SocketMiddelware>(socket));
        }
    }



}
