﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace Analysis.Unitlity._WebSocket.SocketsManager
{
    public class ConnectionManager
    {
        private ConcurrentDictionary<string, WebSocket> _connections = new ConcurrentDictionary<string, WebSocket>();

        public WebSocket GetSocketById(string id)
        {
            return _connections.FirstOrDefault(x => x.Key == id).Value;
        }

        public ConcurrentDictionary<string, WebSocket> GetAllConnections()
        {
            return _connections;
        }

        public string GetId(WebSocket socket)
        { 
            return _connections.FirstOrDefault(x => x.Value == socket).Key;
        }

        /// <summary>
        /// 删除服务
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task RemoveSoxketAsync(string id)
        {
            _connections.TryRemove(id, out var socket);
            await socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "socket connection closed" ,CancellationToken.None);
        }

        /// <summary>
        /// 添加服务
        /// </summary>
        /// <param name="socket"></param>
        public void AddSocket(WebSocket socket) 
        {
            _connections.TryAdd(GetConnectionId(), socket);
        }

        /// <summary>
        /// 生产全球唯一标识符 (GUID) 是一个字母数字标识符
        /// </summary>
        /// <returns></returns>
        private string GetConnectionId()
        {
            return Guid.NewGuid().ToString("N");
        }
    }
}
