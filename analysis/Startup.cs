using Analysis.Controllers;
using Analysis.IRepository;
using Analysis.IService;
using Analysis.Repository;
using Analysis.Service;
using Analysis.Unitlity._WebSocket.Handles;
using Analysis.Unitlity._WebSocket.SocketsManager;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using SqlSugar.IOC;
using System;
using System.Collections.Generic;
using System.Text;

namespace analysis
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "analysis", Version = "v1" });

                #region Swagger使用JWT鉴权组件
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Description = "直接在下拉框中输入Bearer {Token} (注意两者之间有一个空格)",
                    Name = "Authorization",
                    BearerFormat = "JWT",
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[]{ }
                    }
                });
                #endregion

            });

            //string a = this.Configuration["SqlConn"];

            #region SqlSugar.IOC
            
            services.AddSqlSugar(new IocConfig()
            {
                DbType = IocDbType.MySql,
                ConnectionString = this.Configuration["SqlConn"],
                IsAutoCloseConnection = true, //自动释放
                ConfigId = "0",
                //从库
                SlaveConnectionConfigs = new List<IocConfig>() {
                     new IocConfig() {  ConfigId = "1",  ConnectionString=this.Configuration["SqlConn2"] } ,
                     new IocConfig() {  ConfigId = "2",  ConnectionString=this.Configuration["SqlConn3"] }
                }
            });           
            #endregion

            #region IOC依赖注入
            services.AddCustomIOC();
            #endregion


            #region JWT鉴权依赖注入
            services.AddCustomJWT();
            #endregion

            #region Websocket依赖注入
            services.AddWebSocketManager();
            #endregion

            #region MemoryCache缓存依赖注入
            services.AddMemoryCache();
            #endregion


            //services.Configure<ExcelHandleController>(Configuration);
            //services.AddControllersWithViews();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        //public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env,IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "analysis v1"));
            }

            app.UseRouting();
              
            // WebSocket
            app.UseWebSockets();
            app.MapSockets("/ws", serviceProvider.GetService<WebSocketMessageHandler>());
            app.UseDefaultFiles();
            app.UseStaticFiles();

            // 添加管道 en鉴权  or授权 
            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }

    /// <summary>
    /// SqlSugar IOC  JWT
    /// </summary>
    public static class  IOCExtend 
    {
        public static IServiceCollection AddCustomIOC(this IServiceCollection services)
        {
            services.AddScoped<IProjectsRepository, ProjectsRepository>();
            services.AddScoped<IProjectsService, ProjectsService>();

            services.AddScoped<ISubSystemRepository, SubSystemRepository>();
            services.AddScoped<ISubSystemService, SubSystemService>();

            services.AddScoped<IMachineRepository, MachineRepository>();
            services.AddScoped<IMachineService, MachineService>();

            services.AddScoped<ITelemetryRepository, TelemetryRepository>();
            services.AddScoped<ITelemetryService, TelemetryService>();

            services.AddScoped<ITjsonRepository, TjsonRepository>();
            services.AddScoped<ITjsonService, TjsonService>();

            services.AddScoped<IBaseInfoRepository, BaseInfoRepository>();
            services.AddScoped<IBaseInfoService, BaseInfoService>();

            return services;
        }

        public static IServiceCollection AddCustomJWT(this IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("SDMC-CJASI-SAD-DFSFA-SADHJVF-VF")),
                        ValidateIssuer = true ,
                        ValidIssuer= "http://localhost:6060",
                        ValidateAudience = true,
                        ValidAudience = "http://localhost:81",
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.FromMinutes(60)
                    };
                });
            return services;
        }
    }
     
}
