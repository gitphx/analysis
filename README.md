### 1.项目效果

![image-20220110172652172](README.assets/image-20220110172652172.png)

### 2.项目介绍

```markdown
# 基于asp.net.core 5.0 API框架开发

# 数据库使用mysql ,由SqlSugar数据操作管理

#
```



### 3.项目结构

```
- API交互层
	- 控制器
- model 层
	- 数据（表）模型
- service层数据处理层
	- IService
	- Service
- repository仓储层
	- IRepository
	- Repository
```



![image-20220110172226811](README.assets/image-20220110172226811.png)

### 4.项目引用

```markdown
```









### 5.项目更新logs