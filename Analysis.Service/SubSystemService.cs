﻿using Analysis.IRepository;
using Analysis.IService;
using Analysis.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analysis.Service
{
    public class SubSystemService:BaseService<SubSystem>,ISubSystemService
    {
        private readonly ISubSystemRepository _iSubSystemRepository;
        public SubSystemService(ISubSystemRepository iSubSystemRepository)
        {
            base._iBaseRepository = iSubSystemRepository;
            _iSubSystemRepository = iSubSystemRepository;
        }
    }
}
