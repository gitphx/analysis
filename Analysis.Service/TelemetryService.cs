﻿using Analysis.IRepository;
using Analysis.IService;
using Analysis.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analysis.Service
{
    public class TelemetryService:BaseService<Telemetry>,ITelemetryService
    {
        private readonly ITelemetryRepository _iTelemetryRepository;
        public TelemetryService(ITelemetryRepository iTelemetryRepository)
        {
            base._iBaseRepository = iTelemetryRepository;
            _iTelemetryRepository = iTelemetryRepository;
        }
    }
}
