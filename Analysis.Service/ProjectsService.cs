﻿using Analysis.IRepository;
using Analysis.IService;
using Analysis.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analysis.Service
{
    public class ProjectsService : BaseService<Projects>, IProjectsService
    {
        private readonly IProjectsRepository _iProjectsRepository;
        public ProjectsService(IProjectsRepository iProjectsRepository)
        {
            base._iBaseRepository = iProjectsRepository;
            _iProjectsRepository = iProjectsRepository;
        }
    }
}
