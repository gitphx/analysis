﻿using Analysis.IRepository;
using Analysis.IService;
using Analysis.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analysis.Service
{
    public class MachineService:BaseService<Machine>,IMachineService
    {
        private readonly IMachineRepository _iMachineRepository;
        public MachineService(IMachineRepository iMachineRepository)
        {
            base._iBaseRepository = iMachineRepository;
            _iMachineRepository = iMachineRepository;
        }
    }
}
