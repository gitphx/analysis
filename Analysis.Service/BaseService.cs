﻿using Analysis.IRepository;
using Analysis.IService;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Analysis.Service
{
    public class BaseService<T> : IBaseService<T> where T : class, new()
    {
        // 从子类构造函数传入
        protected IBaseRepository<T> _iBaseRepository; 
        public async Task<bool> CreateAsync(T entity)
        {
           return await _iBaseRepository.CreateAsync(entity);
        }

        public async Task<bool> CreateListAsync(List<T> entity)
        {
            return await _iBaseRepository.CreateListAsync(entity);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            return await _iBaseRepository.DeleteAsync(id);
        }

        public async Task<bool> EditAsync(T entity)
        {
            return await _iBaseRepository.EditAsync(entity);
        }

        public  async Task<T> FindAsync(int id)
        {
            return await _iBaseRepository.FindAsync(id);
        }

        public async Task<T> FindAsync(Expression<Func<T, bool>> func)
        {
            return await _iBaseRepository.FindAsync(func);
        }

        public async Task<List<T>> QueryAsync()
        {
            return await _iBaseRepository.QueryAsync();
        }

        public async Task<List<T>> QueryAsync(Expression<Func<T, bool>> func)
        {
            return await _iBaseRepository.QueryAsync(func);
        }

        public async Task<List<T>> QueryAsync(int page, int size, RefAsync<int> total)
        {
            return await _iBaseRepository.QueryAsync(page, size,total);
        }

        public async Task<List<T>> QueryAsync(Expression<Func<T, bool>> func, int page, int size, RefAsync<int> total)
        {
            return await _iBaseRepository.QueryAsync(func, page, size, total);
        }
    }
}
