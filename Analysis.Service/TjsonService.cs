﻿using Analysis.IRepository;
using Analysis.IService;
using Analysis.Model;

namespace Analysis.Service
{
    public class TjsonService:BaseService<Tjson>, ITjsonService
    {
        private readonly ITjsonRepository _iTjsonRepository;

        public TjsonService(ITjsonRepository tjsonService )
        {
            base._iBaseRepository = tjsonService;
            _iTjsonRepository = tjsonService;
        }
    }
}
