﻿using Analysis.IRepository;
using Analysis.IService;
using Analysis.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analysis.Service
{
    public class BaseInfoService:BaseService<BaseInfo>,IBaseInfoService
    {
        private readonly IBaseInfoRepository _iBaseInfoRepository;
        public BaseInfoService(IBaseInfoRepository iBaseInfoRepository)
        {
            base._iBaseRepository = iBaseInfoRepository;
            _iBaseInfoRepository = iBaseInfoRepository;
        }
    }
}
