﻿using Analysis.IRepository;
using Analysis.Model;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Analysis.Repository
{
    public class SubSystemRepository:BaseRepository<SubSystem>,ISubSystemRepository
    {
        public async override Task<List<SubSystem>> QueryAsync()
        {
            return await base.Context.Queryable<SubSystem>()
                .Mapper(
                    s=>s.MachineInfo,
                    s=> s.MachineInfo.First().Msid)
                .Mapper(
                    s=>s.TelemetryInfo,
                    s=> s.TelemetryInfo.First().Subid)
                .ToListAsync();
        }

        public async override Task<List<SubSystem>> QueryAsync(Expression<Func<SubSystem, bool>> func)
        {
            return await base.Context.Queryable<SubSystem>()
                .Where(func)
                .Mapper(
                    s => s.MachineInfo,
                    s => s.MachineInfo.First().Msid)
                .Mapper(
                    s => s.TelemetryInfo,
                    s => s.TelemetryInfo.First().Subid)
                .ToListAsync();
        }
    }
}
