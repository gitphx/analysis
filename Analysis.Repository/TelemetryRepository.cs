﻿using Analysis.IRepository;
using Analysis.Model;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Analysis.Repository
{
    public class TelemetryRepository : BaseRepository<Telemetry>, ITelemetryRepository
    {
        public async override Task<List<Telemetry>> QueryAsync(int page, int size,RefAsync<int> total)
        {
            return await base.Context.Queryable<Telemetry>()
                .ToPageListAsync(page, size,total);
        }

        public async override Task<List<Telemetry>> QueryAsync(Expression<Func<Telemetry,bool>> func, int page, int size, RefAsync<int> total)
        {
            return await base.Context.Queryable<Telemetry>()
                .Where(func)
                .ToPageListAsync(page, size, total);
        }
    }
}
