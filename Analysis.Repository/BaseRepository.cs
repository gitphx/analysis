﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Analysis.IRepository;
using Analysis.Model;
using SqlSugar;
using SqlSugar.IOC;

namespace Analysis.Repository
{
    public class BaseRepository<T> : SimpleClient<T>, IBaseRepository<T> where T : class, new()
    {
        public BaseRepository(ISqlSugarClient context = null) : base(context)
        {
            base.Context = DbScoped.SugarScope;

            // 创建数据库
            // base.Context.DbMaintenance.CreateDatabase();
            // 创建表

            base.Context.CodeFirst.SplitTables().InitTables<Tjson>();
            //base.Context.CodeFirst.InitTables(
            //    typeof(Projects),
            //    typeof(SubSystem),
            //    typeof(Machine),
            //    typeof(BaseInfo),
            //    typeof(Telemetry)
            //);
        }
        public async Task<bool> CreateAsync(T entity)
        {
            return await base.InsertAsync(entity);
        }

        public async Task<bool> CreateListAsync(List<T> entity)
        {
            return await base.InsertRangeAsync(entity);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            return await base.DeleteByIdAsync(id);
        }

        public async Task<bool> EditAsync(T entity)
        {
            return await base.UpdateAsync(entity);
        }

        public virtual async Task<T> FindAsync(int id)
        {
            return await base.GetByIdAsync(id);
        }

        public virtual async Task<T> FindAsync(Expression<Func<T, bool>> func)
        {
            return await base.GetSingleAsync(func);
        }

        public virtual async Task<List<T>> QueryAsync()
        {
            return await base.GetListAsync();
        }

        public virtual async Task<List<T>> QueryAsync(System.Linq.Expressions.Expression<Func<T, bool>> func)
        {
            return await base.GetListAsync(func);
        }

        public virtual async Task<List<T>> QueryAsync(int page, int size, RefAsync<int> total)
        {
            return await base.Context.Queryable<T>()
                .ToPageListAsync(page,size,total);
        }

        public virtual async Task<List<T>> QueryAsync(System.Linq.Expressions.Expression<Func<T, bool>> func, int page, int size, RefAsync<int> total)
        {
            return await base.Context.Queryable<T>()
               .Where(func)
               .ToPageListAsync(page, size, total);
        }
    }
}
