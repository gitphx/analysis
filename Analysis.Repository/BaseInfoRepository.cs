﻿using Analysis.IRepository;
using Analysis.Model;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Analysis.Repository
{
    public class BaseInfoRepository:BaseRepository<BaseInfo>,IBaseInfoRepository
    {
        public async override Task<List<BaseInfo>> QueryAsync(int page, int size, RefAsync<int> total)
        {
            return await base.Context.Queryable<BaseInfo>()
                .ToPageListAsync(page, size, total);
        }

        public async override Task<List<BaseInfo>> QueryAsync(Expression<Func<BaseInfo, bool>> func, int page, int size, RefAsync<int> total)
        {
            return await base.Context.Queryable<BaseInfo>()
                .Where(func)
                .ToPageListAsync(page, size, total);
        }
    }
}
